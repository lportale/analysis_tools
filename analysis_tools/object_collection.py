class ObjectCollection():
    def __init__(self, objects=[]):
        if not isinstance(object, list):
            objects = list(objects)
        object_names = [obj.name for obj in objects]
        self.verify(object_names)
        self.objects = objects
    
    def __iter__(self):
        for obj in self.objects:
            yield obj

    def __add__(self, other_collection):
        tmp_coll = ObjectCollection(self.objects)
        for obj in other_collection.objects:
            tmp_coll.add(obj)
        return tmp_coll

    def __iadd__(self, other_collection):
        # for obj in other_collection.objects:
            # self.add(obj)
        # return self
        return self.__add__(other_collection)

    def __repr__(self):
        return "ObjectCollection(%s)" % self.objects

    def __getitem__(self, i):
        return self.objects[i]

    def __len__(self):
        return len(self.objects)
    
    def get(self, name):
        for obj in self.objects:
            if obj.name == name:
                return obj
        raise ValueError("No object found with name " + name)
    
    def add(self, new_object):
        object_names = [obj.name for obj in self.objects] + [new_object.name]
        self.verify(object_names)
        self.objects.append(new_object)

    def append(self, new_object):
        self.add(new_object)

    def verify(self, object_names):
        for name in object_names:
            if object_names.count(name) > 1:
                raise ValueError("Two or more objects have the same name %s" % name)
        return True
    
    def names(self):
        return [obj.name for obj in self.objects]
